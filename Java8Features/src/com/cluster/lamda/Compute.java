package com.cluster.lamda;

@FunctionalInterface
public interface Compute 
{
	abstract int compute(int a, int b);
}
