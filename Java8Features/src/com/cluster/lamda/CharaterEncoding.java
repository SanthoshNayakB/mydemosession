package com.cluster.lamda;

import java.util.ArrayList;
import java.util.List;


public class CharaterEncoding {

	public List<byte[]> SendOtherLangSMS(String SMS) {

		List<byte[]> list = new ArrayList<byte[]>();
		byte[] binarySMS = new byte[SMS != null ? SMS.length() * 2 : 0];
		short unicodeValue = 0x00;
		int i = 0x00, j = 0x00;

		char[] tokenBytes = SMS.toCharArray();
		String strDecimal;
		try {
			int tokenBytesLength = tokenBytes.length;
			while (i < tokenBytesLength) {
				if (i + 6 < tokenBytesLength && tokenBytes[i] == '&' && tokenBytes[i + 1] == '#'
						&& tokenBytes[i + 6] == ';') {
					i += 2;
					strDecimal = new String(tokenBytes, i, 4);
					unicodeValue = Short.parseShort(strDecimal);
					mcfn_encodeShortToByteMSB(unicodeValue, binarySMS, j);
					i += 5; // skipping 4 digits + ';'
					j += 2;

				} else {
					binarySMS[j] = 0x00;
					j++;
					mcfn_encodeByte(tokenBytes[i], binarySMS, j);
					i++;
					j++;
				}
			}

			//if (j <= 140) {
				byte[] finalBytes = new byte[j];
				for (int k = 0; k < j; k++) {
					finalBytes[k] = binarySMS[k];
				}
				list.add(finalBytes);

			/*} else {
				int Index = 0x01;
				i = 0;
				while (i < j) {
					byte[] finalBytes = new byte[140];
					int k = 0;
					finalBytes[k++] = 0x05;
					finalBytes[k++] = 0x00;
					finalBytes[k++] = 0x03;
					finalBytes[k++] = 0x00;
					finalBytes[k++] = (byte) ((j / 134) + ((j % 134) != 0x00 ? 0x01 : 0x00));
					finalBytes[k++] = (byte) Index++;

					for (; i < j && k < 134; i++, k++) {
						finalBytes[k] = binarySMS[i];
					}
					byte[] b = new byte[k];
					System.arraycopy(finalBytes, 0, b, 0, k);
					list.add(b);
				}
			}*/

		} catch (Exception e) {
			System.out.println("Exception in Arabic Convertor ,Exception " + e.getMessage());
			list.add("System Busy.Please try again.".getBytes());
		}
		return list;

	}

	public static String toHex(byte[] digest) {
		String digits = "0123456789abcdef";
		StringBuilder sb = new StringBuilder(digest.length * 2);
		for (byte b : digest) {
			int bi = b & 0xff;
			sb.append(digits.charAt(bi >> 4));
			sb.append(digits.charAt(bi & 0xf));
		}
		return sb.toString();
	}

	public void mcfn_encodeShortToByteMSB(int siL_Value, byte[] pucL_Byte, int siL_Index) {
		pucL_Byte[siL_Index++] = (byte) (siL_Value >> 8);
		pucL_Byte[siL_Index++] = (byte) siL_Value;
	}

	public void mcfn_encodeByte(int siL_Value, byte[] pucL_Byte, int siL_Index) {
		pucL_Byte[siL_Index] = (byte) siL_Value;
	}

}
