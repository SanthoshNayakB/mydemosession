package com.cluster.lamda;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.cluster.methodrefer.Globals;

public class Demo {

	
	
	public static void main(String[] args) throws ParseException 
	{
		/*SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf1.parse(sdf1.format(new Date())));
		cal.add(Calendar.DATE, Integer.parseInt("30"));
		
		String nextBilling=sdf1.format(cal.getTime());
		System.out.println("NextBilling date::::"+ nextBilling);
		String diff= String.valueOf(getDateDiff(nextBilling));
		System.out.println("Value of diff is ::::"+ diff);*/
		
		DecimalFormat df1 	= new DecimalFormat(Globals.BILLING_DECIMAL_FORMAT);
		String fee = convert(df1.format(Double.parseDouble("10.0")),"97433655607");
		System.out.println("Feeeee:::::"+ fee);
		
		Object[] obj = new Object[] {
				fee != null ? fee: ""};
		System.out.println("Obj::::"+ obj);
	}
	
	@SuppressWarnings("static-access")
	public static String convert(String str,String msisdn) {
		String finalString=null;
		List<byte[]> list = null;
		CharaterEncoding encoding = new CharaterEncoding();
		StringBuffer sb = new StringBuffer();
		System.out.println("Original Sting :"+ str);
		try{
			list=encoding.SendOtherLangSMS(str);
			for(int i=0;i<list.size();i++){
				byte[] finalBytes =list.get(i);
				
				sb.append(encoding.toHex(finalBytes));
			}
			finalString=sb.toString();
			System.out.println("Converted String "+ finalString);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error"+ e);
		}finally{
			sb=null;
			encoding=null;
		}
		return finalString;
	}
	
	/*public static int getDateDiff(String nextBilling) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		int dateDiff = 0;
		Date startDate = null;
		Date currDate = null;
		try {
			startDate = dateFormat.parse(nextBilling);
			currDate = dateFormat.parse(dateFormat.format(new Date()));

			dateDiff = (int) (startDate.getTime() - currDate.getTime())/ (60 * 60  * 24 * 1000);
			startDate = null;
			currDate = null;
		} catch (Exception e) {

		} finally {
			currDate = null;
			startDate = null;
		}
		return dateDiff;
	}*/
	
	public static long getDateDiff(String nextBilling) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		long dateDiff = 0;
		Date startDate = null;
		Date currDate = null;
		try {
			
			startDate = dateFormat.parse(nextBilling);
			currDate = dateFormat.parse(dateFormat.format(new Date()));

			dateDiff = (long) (startDate.getTime() - currDate.getTime())/ (60 * 60 * 24 * 1000);
			startDate = null;
			currDate = null;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			currDate = null;
			startDate = null;
		}
		return dateDiff;
	}

}
