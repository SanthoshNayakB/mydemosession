package com.cluster.lamda;

public class LamdaDemo {

	public static void main(String[] args) 
	{
		System.out.println("Begin of main method");
		Compute addition= (a,b) -> (a+b);
		Compute mul= (a,b) -> (a*b);
		Compute subtract = (a, b) -> (a-b);
		System.out.println(addition);
		System.out.println("Result:::"+ addition.compute(10, 20));
		System.out.println("Result:::"+ mul.compute(10, 20));
		System.out.println("Result ::::"+ subtract.compute(40, 15));
		System.out.println("End of main method");
	}
}
