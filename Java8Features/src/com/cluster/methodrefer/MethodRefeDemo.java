package com.cluster.methodrefer;

public class MethodRefeDemo {
	
	public static void saySomething()
	{
		System.out.println("Inside static method");
	}

	public static void main(String[] args) 
	{
		Sayable sayable = MethodRefeDemo::saySomething;  
        sayable.say();  
	}

}
