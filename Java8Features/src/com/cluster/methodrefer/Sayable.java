package com.cluster.methodrefer;

public interface Sayable 
{
	public void say();
}
